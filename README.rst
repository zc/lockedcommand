lockcommand
***********

A wrapper script that uses a lock file to prevent multiple
simultaneous executions of a command.  For example, to prevent
sleeping simultaneously::

  lockcommand locfile sleep 1

:)


Changes
*******

0.1.0 (yyyy-mm-dd)
==================

Initial release
