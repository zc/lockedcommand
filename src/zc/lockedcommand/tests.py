from zope.testing import setupstack
import doctest


def test_suite():
    return doctest.DocFileSuite(
        'main.test',
        setUp=setupstack.setUpDirectory,
        tearDown=setupstack.tearDown,
        )

