"""Usage: %prog [options] lockefile_path command argument1 ...
"""
import logging
import optparse
import os
import sys
import zc.lockfile

parser = optparse.OptionParser(__doc__)
parser.add_option(
    '-q', '--quiet', action='store_true',
    help="Don't alert or spew if already locked")

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    options, args = parser.parse_args(args)
    lockfile_path = args.pop(0)

    logging.basicConfig(level=logging.CRITICAL)

    try:
        lockfile = zc.lockfile.LockFile(lockfile_path)
    except zc.lockfile.LockError:
        if not options.quiet:
            print '@@ALERT@@ MINOR: lockedcommand %r %r' % (
                lockfile_path, args)
        return 1

    try:
        if os.system(' '.join(args)):
            raise SystemError(args)
    finally:
        lockfile.close()
